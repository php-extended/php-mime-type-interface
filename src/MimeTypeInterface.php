<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use Stringable;

/**
 * MimeTypeInterface interface file.
 * 
 * This interface defines a single mime type.
 * 
 * @author Anastaszor
 */
interface MimeTypeInterface extends Stringable
{
	
	/**
	 * Gets the category of this mime type.
	 * 
	 * @return MimeCategoryInterface
	 */
	public function getMimeCategory() : MimeCategoryInterface;
	
	/**
	 * Gets the name of this mime type.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets a canonical representation for this mime type. This should be a
	 * standardized key for this mime type, that includes the representation
	 * of its category.
	 * 
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets the standard known extensions for this mime type.
	 * 
	 * @return array<integer, string>
	 */
	public function getExtensions() : array;
	
	/**
	 * Gets whether this mime type equals another mime type.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $mimeType
	 * @return boolean
	 */
	public function equals($mimeType) : bool;
	
}
