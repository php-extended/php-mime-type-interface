<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use Stringable;

/**
 * MimeCategoryInterface interface file.
 * 
 * This interface represents a category of mime types.
 * 
 * @author Anastaszor
 */
interface MimeCategoryInterface extends Stringable
{
	
	/**
	 * Gets the name of this mime type.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the string that represents this mime category. This should be a
	 * standardized name for this category.
	 * 
	 * @return string
	 */
	public function getCanonicalRepresentation() : string;
	
	/**
	 * Gets all the known types for this category.
	 * 
	 * @return array<integer, MimeTypeInterface>
	 */
	public function getMimeTypes() : array;
	
	/**
	 * Gets whether this mime type equals another mime type.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $mimeCategory
	 * @return boolean
	 */
	public function equals($mimeCategory) : bool;
	
}
