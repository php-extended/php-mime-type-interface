<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use PhpExtended\Parser\ParserInterface;

/**
 * MimeTypeParserInterface interface file.
 * 
 * This class represents a parser for mime types.
 * 
 * @author Anastaszor
 * @extends ParserInterface<MimeTypeInterface>
 */
interface MimeTypeParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
