<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use Throwable;

/**
 * UnavailableCategoryThrowable interface file.
 * 
 * This is thrown when a category is requested but is not known by a provider.
 * 
 * @author Anastaszor
 */
interface UnavailableCategoryThrowable extends Throwable
{
	
	// nothing to add
	
}
