<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use Stringable;

/**
 * MimeTypeProvider class file.
 * 
 * This class represents a provider for mime types that are known. All the
 * implementing providers must implement search methods that are case
 * insensitive (even if they are called "exact match").
 * 
 * @author Anastaszor
 */
interface MimeTypeProviderInterface extends Stringable
{
	
	/**
	 * Gets all the categories that exist for this provider.
	 * 
	 * @return array<integer, MimeCategoryInterface>
	 */
	public function getAllCategories() : array;
	
	/**
	 * Gets all the mime types that exist for the given category for this
	 * provider.
	 * 
	 * @param ?MimeCategoryInterface $category
	 * @return array<integer, MimeTypeInterface>
	 */
	public function getMimeTypesForCategory(?MimeCategoryInterface $category) : array;
	
	/**
	 * Gets all the mime types that use the given extension.
	 * 
	 * @param ?string $extension
	 * @return array<integer, MimeTypeInterface>
	 */
	public function getMimeTypesForExtension(?string $extension) : array;
	
	/**
	 * Gets a mime category that exactly fits the given category, or throws
	 * an exception if such mime category is not known to exist.
	 * 
	 * @param ?string $category
	 * @return MimeCategoryInterface
	 * @throws UnavailableCategoryThrowable
	 */
	public function getExactCategory(?string $category) : MimeCategoryInterface;
	
	/**
	 * Gets the mime type that exactly fits the given mime type, or throws
	 * an exception if such mime type is not known to exist.
	 * 
	 * @param ?string $mimeType
	 * @return MimeTypeInterface
	 * @throws UnavailableMimeTypeThrowable
	 */
	public function getExactMimeType(?string $mimeType) : MimeTypeInterface;
	
	/**
	 * Gets a mime category that best fits the given category string as for the
	 * provider's knowledge to a valid mime category.
	 * 
	 * @param ?string $category
	 * @return MimeCategoryInterface
	 */
	public function getBestMatchCategory(?string $category) : MimeCategoryInterface;
	
	/**
	 * Gets a mime type that best fits the given mime type string as for the
	 * provider's knowledge to a valid mime type.
	 * 
	 * @param ?string $mimeType
	 * @return MimeTypeInterface
	 */
	public function getBestMatchMimeType(?string $mimeType) : MimeTypeInterface;
	
	/**
	 * Gets a mime type that best fits the given extension as for the 
	 * provider's knowledge to a valid mime type.
	 * 
	 * @param ?string $extension
	 * @return MimeTypeInterface
	 */
	public function getBestMatchForExtension(?string $extension) : MimeTypeInterface;
	
}
